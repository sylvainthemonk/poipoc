package fr.poipoc;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by slemoine on 16/09/15.
 */
public class ExcelWriter {

    //header
    private static final String[] headerTitles = {"REFERENCE REPERAGE","NIVEAU-PP-LOC","TYPE LOC","LOCALISATION PRECISE,TYPE_COMPO-COMPO-PARTIE_COMPO",
            "INFO COMPO","TYPE_REPERAGE","DATE_REPERAGE","AMIANTE","TYPE_AMIANTE","ORIGINE RESULTAT","REFERENCE LABO","CONCLUSION"};

    //niveau-partie-locaisation
    private static final String [] listNiveauPpLoc = {"Faible/Pp01/Loc01", "Fort/Pp01/Loc02", "Moyen/Pp02/Loc02"};

    //type composant - composant - partie composant
    private static final String [] listTypeCompoCompoPartieCompo = {"TC01/C01/PC01", "TC02/C02/PC02"};

    private static int maxRow = 65000;

    public static void main(String[] args) throws IOException {

        XSSFWorkbook wb = new XSSFWorkbook();

        XSSFSheet sh = wb.createSheet("Sheet1");

        Row row = sh.createRow(0);

        for(int cellnum = 0; cellnum < headerTitles.length; cellnum++){
            Cell cell = row.createCell(cellnum);
            cell.setCellValue(headerTitles[cellnum]);
        }

        //création helper
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sh);

        //list Niveau,partie privative,localisation
        XSSFDataValidationConstraint dvcNiveau = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(listNiveauPpLoc);
        //Ligne 1 to 65000, col 1 to 0
        CellRangeAddressList addressListNiveau = new CellRangeAddressList(1,maxRow,1,1);

        XSSFDataValidation validationNiveau = (XSSFDataValidation)dvHelper.createValidation(dvcNiveau, addressListNiveau);

        //list Type composant, composant, partie composant
        XSSFDataValidationConstraint dvcTypeComposant = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(listTypeCompoCompoPartieCompo);

        //Ligne 1 to 65000, col 4 to 0
        CellRangeAddressList addressListComposant = new CellRangeAddressList(1,maxRow,3,3);

        XSSFDataValidation validationComposant = (XSSFDataValidation)dvHelper.createValidation(dvcTypeComposant, addressListComposant);

        sh.addValidationData(validationNiveau);
        sh.addValidationData(validationComposant);

        try(FileOutputStream out = new FileOutputStream("outputwrite.xlsx")){
            wb.write(out);
            out.close();
        }finally {
            wb.close();
        }
    }
}
