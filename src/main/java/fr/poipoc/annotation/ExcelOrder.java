package fr.poipoc.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by slemoine on 27/09/15.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelOrder {

    int order();
}
