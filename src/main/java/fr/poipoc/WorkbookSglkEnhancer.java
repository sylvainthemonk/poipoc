package fr.poipoc;

import fr.poipoc.dto.DatasheetDTO;
import fr.poipoc.dto.IExcelPocDTO;
import fr.poipoc.dto.PocDTO;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by slemoine on 18/09/15.
 */
public class WorkbookSglkEnhancer {


    /**
     * Ecrit une feuille de données sur 2 colonnes dans le workbook
     * @param sheetName
     * @param data
     * @return
     */
    public static Sheet writeDataValidationListSheet(Workbook workbook, DatasheetDTO datasheet){

        Sheet sheet = workbook.createSheet(datasheet.getSheetName());

        int i = 0;

        if(datasheet.getHeaderTitle() != null){
            Row row = sheet.createRow(i++);

            for(int j=0;j<datasheet.getHeaderTitle().length;j++){
                Cell cell = row.createCell(j);
                cell.setCellValue(datasheet.getHeaderTitle()[j]);
            }
        }

        for(IExcelPocDTO dto : datasheet.getData()) {

            Row row = sheet.createRow(i++);

            //La colonne pour les ids
            Number[] ids = dto.getIds();

            for(int j=0 ; j<ids.length;j++){
                Cell cell = row.createCell(j);
                cell.setCellValue(ids[j].doubleValue());
            }

            //La colonne pour le label
            Cell cellLabels = row.createCell(ids.length);
            cellLabels.setCellValue(dto.getLabel());
        }

        return sheet;
    }


    /**
     * Applique une formula sur une colonne
     * @param sheetName
     * @param column
     */
    public static DataValidationConstraint getFormulaListConstraint(DataValidationHelper dvHelper, Sheet sheet, int columnSource){

        if(dvHelper == null){
            throw new IllegalArgumentException("DV helper must be set");
        }

        char colLetter = (char) (columnSource + 65);

        DataValidationConstraint dvc = dvHelper.createFormulaListConstraint(sheet.getSheetName() + "!" + colLetter + ":" + colLetter);

        return dvc;
    }

    public static void applyListContraint(Sheet sheet, DataValidationHelper dvHelper, DataValidationConstraint dvc, int startRow, int endRow, int startColumn, int endColumn){
        CellRangeAddressList addressListNiveau = new CellRangeAddressList(startRow,endRow,startColumn,endColumn);

        DataValidation dv = dvHelper.createValidation(dvc, addressListNiveau);

        sheet.addValidationData(dv);
    }


    public static void main(String args[]) throws IOException {


        final String[] headerTitles = {"REFERENCE REPERAGE","NIVEAU-PP-LOC","TYPE LOC","LOCALISATION PRECISE,TYPE_COMPO-COMPO-PARTIE_COMPO",
                "INFO COMPO","TYPE_REPERAGE","DATE_REPERAGE","AMIANTE","TYPE_AMIANTE","ORIGINE RESULTAT","REFERENCE LABO","CONCLUSION"};

        final String [] listNiveauPpLoc = {"Faible/Pp01/Loc01", "Fort/Pp01/Loc02", "Moyen/Pp02/Loc02"};

        final String [] listTypeCompoCompoPartieCompo = {"TC01/C01/PC01", "TC02/C02/PC02"};

        XSSFWorkbook wb = new XSSFWorkbook();

        XSSFSheet sh = wb.createSheet("CLIENT");

        Row row = sh.createRow(0);

        for(int cellnum = 0; cellnum < headerTitles.length; cellnum++){
            Cell cell = row.createCell(cellnum);
            cell.setCellValue(headerTitles[cellnum]);
        }

        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sh);

        Map<String, String> map = new HashMap<>();

        map.put("toto","titi");

        DatasheetDTO dataSheetDTO = new DatasheetDTO();

        List<IExcelPocDTO> l = new ArrayList<>();

        PocDTO pocDTO1 = new PocDTO();
        pocDTO1.setId1(1L);
        pocDTO1.setId2(2L);
        pocDTO1.setId3(3L);
        pocDTO1.setLabel1("labe11");
        pocDTO1.setLabel2("labe12");
        pocDTO1.setLabel3("labe13");

        l.add(pocDTO1);

        dataSheetDTO.setHeaderTitle(new String[]{"id1","id2","id3","label"});
        dataSheetDTO.setSheetName("data1");
        dataSheetDTO.setHeaderTitle(new String[]{"id1","id2","id3","label"});
        dataSheetDTO.setData(l);

        Sheet listSheet = writeDataValidationListSheet(wb,dataSheetDTO);

        DataValidationConstraint dvc =getFormulaListConstraint(dvHelper, listSheet, 3);

        applyListContraint(sh,dvHelper,dvc,1,65000,0,0);


        try(FileOutputStream out = new FileOutputStream("outputwrite.xlsx")){
            wb.write(out);
            out.close();
        }finally {
            wb.close();
        }
    }

}
