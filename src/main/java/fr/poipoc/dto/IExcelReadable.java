package fr.poipoc.dto;

/**
 * Created by slemoine on 27/09/15.
 */
public interface IExcelReadable {

    Integer getLineNumber();

    void setLineNumber(Integer lineNumber);
}
