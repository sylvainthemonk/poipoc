package fr.poipoc.dto;

/**
 * Created by slemoine on 21/09/15.
 */
public interface IExcelPocDTO<T extends Number>{

    public T[] getIds();

    public String getLabel();

}
