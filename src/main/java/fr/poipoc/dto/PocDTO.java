package fr.poipoc.dto;

import fr.poipoc.annotation.ExcelOrder;

/**
 * Created by slemoine on 21/09/15.
 */
public class PocDTO implements IExcelPocDTO<Long>, IExcelReadable {

    private Integer lineNumber;

    private Long id1;


    private Long id2;


    private Long id3;

    private String label1;

    private String label2;

    private String label3;

    public Long getId1() {
        return id1;
    }

    @ExcelOrder(order = 0)
    public void setId1(Long id1) {
        this.id1 = id1;
    }


    public Long getId2() {
        return id2;
    }

    @ExcelOrder(order = 1)
    public void setId2(Long id2) {
        this.id2 = id2;
    }

    public Long getId3() {
        return id3;
    }

    @ExcelOrder(order = 2)
    public void setId3(Long id3) {
        this.id3 = id3;
    }

    public String getLabel1() {
        return label1;
    }

    public void setLabel1(String label1) {
        this.label1 = label1;
    }

    public String getLabel2() {
        return label2;
    }

    public void setLabel2(String label2) {
        this.label2 = label2;
    }

    public String getLabel3() {
        return label3;
    }

    public void setLabel3(String label3) {
        this.label3 = label3;
    }

    public Long[] getIds(){
        return new Long[]{getId1(),getId2(),getId3()};
    }

    @Override
    public String getLabel() {
        return label1 + "," + label2 + "," +label3;
    }

    @Override
    public Integer getLineNumber() {
        return lineNumber;
    }

    @Override
    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }
}
