package fr.poipoc.dto;

import java.util.List;

/**
 * Created by slemoine on 21/09/15.
 */
public class DatasheetDTO {

    private String sheetName;

    private String[] headerTitle;

    private int startRowIdx;

    private int startColumnIdx;

    private List<IExcelPocDTO> data;

    public DatasheetDTO(){
        startColumnIdx = 0;
        startRowIdx=0;
    }

    public List<IExcelPocDTO> getData() {
        return data;
    }

    public void setData(List<IExcelPocDTO> data) {
        this.data = data;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String[] getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String[] headerTitle) {
        this.headerTitle = headerTitle;
    }

    public int getStartRowIdx() {
        return startRowIdx;
    }

    public void setStartRowIdx(int startRowIdx) {
        this.startRowIdx = startRowIdx;
    }

    public int getStartColumnIdx() {
        return startColumnIdx;
    }

    public void setStartColumnIdx(int startColumnIdx) {
        this.startColumnIdx = startColumnIdx;
    }

}
