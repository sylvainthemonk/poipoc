package fr.poipoc;

import fr.poipoc.annotation.ExcelOrder;
import fr.poipoc.dto.IExcelReadable;
import fr.poipoc.dto.PocDTO;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by slemoine on 16/09/15.
 */
public class ExcelReader {

    private static void introspect(int order, IExcelReadable obj, Object value) throws IllegalAccessException, InvocationTargetException {

        Class<? extends IExcelReadable> clazz =  obj.getClass();

        Method[] methods = clazz.getMethods();

        for(Method m : methods){
            ExcelOrder a = m.getDeclaredAnnotation(ExcelOrder.class);

            if(a!= null && a.order() == order){

                Parameter p = m.getParameters()[0];

                if(p.getType().equals(Long.class) && value instanceof Double){
                    //Si l'argument est Long on se tente un cast de sauvageon depuis un Double
                    m.invoke(obj,((Double)value).longValue());
                }else{
                    m.invoke(obj,value);
                }
            }
        }
    }

    static <T extends IExcelReadable> void record(List<T> lst, Class<T> clazz){
        if(clazz.equals(PocDTO.class)){
            System.out.println("acaster");
            recordPocDTO(lst.stream().map(e -> (PocDTO) e).collect(Collectors.toList()));
        }
    }

    static <T extends IExcelReadable> void recordPocDTO(List<PocDTO> lst){
        System.out.println("record poc dto");
        //TODO The job !
    }

    static <T extends IExcelReadable> void genericRead(Class<T> clazz) throws IOException, InvocationTargetException, IllegalAccessException, InstantiationException {
        int batchSize = 2;

        try (FileInputStream fis = new FileInputStream("datasheet.xlsx")) {

            Workbook wb = new XSSFWorkbook(fis);

            Sheet sheet = wb.getSheetAt(0);

            System.out.println("Début parsing de la feuille: " + sheet.getSheetName());
            System.out.println("La première ligne de la feuille est à l'index: " + sheet.getFirstRowNum());
            System.out.println("La dernière ligne de la feuille est à l'index: " + sheet.getLastRowNum());

            Row row = null;

            //initialisation au batchSize pour éviter des expansion de la taille mémoire de la liste
            //(pour de la perf)
            List<T> lst = new ArrayList<>(batchSize);

            for (int i = 0; i < sheet.getLastRowNum(); i++) {
                row = sheet.getRow(i);

                if (row == null) {
                    //si ligne null, on passe à la suivante
                    //TODO LOG
                    continue;
                }

                T pocDTO = clazz.newInstance();
                pocDTO.setLineNumber(i);

                //création d'un nouveau DTO
                for (int j = 0; j < row.getLastCellNum(); j++) {

                    if (row.getCell(j) == null) {
                        //cell null on continue
                        System.out.println(String.format("La cellule en (%d,%d) est vide", i, j));
                        continue;
                    }

                    if (row.getCell(j).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                        introspect(j, pocDTO, row.getCell(j).getNumericCellValue());
                    } else {
                        introspect(j, pocDTO, row.getCell(j).getStringCellValue());
                    }
                }

                lst.add(pocDTO);

                if (i == batchSize) {
                    record(lst, clazz);
                }
            }

            wb.close();

            System.out.println("Parsing terminé");

        }
    }

    static public void main(String... args) throws IOException, IllegalAccessException, InvocationTargetException, InstantiationException {

            genericRead(PocDTO.class);


//        int batchSize = 100;
//
//        try(FileInputStream fis = new FileInputStream("datasheet.xlsx")){
//
//            Workbook wb = new XSSFWorkbook(fis);
//
//            Sheet sheet = wb.getSheetAt(0);
//
//            System.out.println("Début parsing de la feuille: "+sheet.getSheetName());
//            System.out.println("La première ligne de la feuille est à l'index: "+sheet.getFirstRowNum());
//            System.out.println("La dernière ligne de la feuille est à l'index: "+sheet.getLastRowNum());
//
//            Row row = null;
//
//            //initialisation au batchSize pour éviter des expansion de la taille mémoire de la liste
//            //(pour de la perf)
//            List<PocDTO> lst = new ArrayList<>(batchSize);
//
//            for(int i=0 ; i< sheet.getLastRowNum(); i++ ){
//                row = sheet.getRow(i);
//
//                if(row == null){
//                    //si ligne null, on passe à la suivante
//                    //TODO LOG
//                    continue;
//                }
//
//                PocDTO pocDTO = new PocDTO();
//                pocDTO.setLineNumber(i);
//
//                //création d'un nouveau DTO
//                for(int j=0; j<row.getLastCellNum();j++){
//
//                    if(row.getCell(j)==null){
//                        //cell null on continue
//                        System.out.println(String.format("La cellule en (%d,%d) est vide",i,j));
//                        continue;
//                    }
//
//                    if(row.getCell(j).getCellType() == Cell.CELL_TYPE_NUMERIC){
//                        introspect(j,pocDTO,row.getCell(j).getNumericCellValue());
//                    }else{
//                        introspect(j,pocDTO,row.getCell(j).getStringCellValue());
//                    }
//                }
//
//                lst.add(pocDTO);
//
//                if(i==batchSize){
//                    record(lst);
//                }
//            }
//
//            wb.close();
//
//            System.out.println("Parsing terminé");
    //    }
    }


}
